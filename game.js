(function() {
    var CSS = {
      arena: {
        width: document.body.offsetWidth < 900 ? document.body.offsetWidth : 900,
        height:
          document.body.offsetHeight < 600 ? document.body.offsetHeight : 600,
        background: "#62247B",
        position: "fixed",
        top: "50%",
        left: "50%",
        zIndex: "999",
        transform: "translate(-50%, -50%)"
      },
      balls: [],
      line: {
        width: 0,
        height: 600,
        borderLeft: "2px dashed #C6A62F",
        position: "absolute",
        top: 0,
        left: "50%"
      },
      stick: {
        width: 12,
        height: 85,
        position: "absolute",
        background: "#C6A62F"
      },
      stick1: {
        left: 0,
        top: 150
      },
      scoreBoard1: {
        color: "#ffffff5c",
        fontSize: "260px",
        position: "absolute",
        top: 100,
        left: "15%"
      },
      scoreBoard2: {
        color: "#ffffff5c",
        fontSize: "260px",
        position: "absolute",
        top: 100,
        right: "15%"
      }
    };
    CSS["stick2"] = {
      left: CSS.arena.width - CSS.stick.width,
      top: 150
    };
  
    var CONSTS = {
      gameSpeed: 20,
      score1: 0,
      score2: 0,
      stick1Speed: 0,
      stick2Speed: 0,
      ballTopSpeed: [],
      ballLeftSpeed: [],
      Stick1UpPressed: false,
      Stick1DownPressed: false,
      Stick2UpPressed: false,
      Stick2DownPressed: false,
      currentBallCount: 0
    };
  
    function start() {
      loadScore();
      draw();
      setEvents();
      addNewBall();
      loop();
    }
  
    function draw() {
      $("<div/>", { id: "pong-game" })
        .css(CSS.arena)
        .appendTo("body");
      $("<div/>", { id: "pong-line" })
        .css(CSS.line)
        .appendTo("#pong-game");
      $("<div/>", { id: "stick-1" })
        .css($.extend(CSS.stick1, CSS.stick))
        .appendTo("#pong-game");
      $("<div/>", { id: "stick-2" })
        .css($.extend(CSS.stick2, CSS.stick))
        .appendTo("#pong-game");
  
      $("<div/>", { id: "score-board-1" })
        .css(CSS.scoreBoard1)
        .appendTo("#pong-game")
        .text(CONSTS.score1);
      $("<div/>", { id: "score-board-2" })
        .css(CSS.scoreBoard2)
        .appendTo("#pong-game")
        .text(CONSTS.score2);
    }
  
    function setEvents() {
      $(document).on("keydown", function(e) {
        if (e.keyCode == 87) {
          CONSTS.stick1Speed = -10;
          CONSTS.Stick1UpPressed = true;
        }
      });
  
      $(document).on("keyup", function(e) {
        if (e.keyCode == 87) {
          CONSTS.stick1Speed = 0;
        }
      });
  
      $(document).on("keydown", function(e) {
        if (e.keyCode == 83) {
          CONSTS.stick1Speed = 10;
          CONSTS.Stick1DownPressed = true;
        }
      });
  
      $(document).on("keyup", function(e) {
        if (e.keyCode == 83) {
          CONSTS.stick1Speed = 0;
        }
      });
  
      $(document).on("keydown", function(e) {
        if (e.keyCode == 38) {
          CONSTS.stick2Speed = -10;
          CONSTS.Stick2UpPressed = true;
        }
      });
  
      $(document).on("keyup", function(e) {
        if (e.keyCode == 38) {
          CONSTS.stick2Speed = 0;
        }
      });
  
      $(document).on("keydown", function(e) {
        if (e.keyCode == 40) {
          CONSTS.stick2Speed = 10;
          CONSTS.Stick2DownPressed = true;
        }
      });
  
      $(document).on("keyup", function(e) {
        if (e.keyCode == 40) {
          CONSTS.stick2Speed = 0;
        }
      });
      $(document).on("keyup", function(e) {
        if (e.keyCode == 32) {
          addNewBall();
        }
      });
    }
  
    function loop() {
      window.pongLoop = setInterval(function() {
        CSS.stick1.top += CONSTS.stick1Speed;
        $("#stick-1").css("top", CSS.stick1.top);
  
        CSS.stick2.top += CONSTS.stick2Speed;
        $("#stick-2").css("top", CSS.stick2.top);
  
        for (let i = 0; i < CSS.balls.length; i++) {
          if ($("#pong-ball-" + i + ":hidden").length) {
            continue;
          }
          CSS.balls[i].top += CONSTS.ballTopSpeed[i];
          CSS.balls[i].left += CONSTS.ballLeftSpeed[i];
  
          if (
            CSS.balls[i].top <= 0 ||
            CSS.balls[i].top >= CSS.arena.height - CSS.balls[i].height
          ) {
            CONSTS.ballTopSpeed[i] = CONSTS.ballTopSpeed[i] * -1;
          }
  
          $("#pong-ball-" + [i]).css({
            top: CSS.balls[i].top,
            left: CSS.balls[i].left
          });
  
          if (CSS.balls[i].left <= CSS.stick.width) {
            (CSS.balls[i].top > CSS.stick1.top &&
              CSS.balls[i].top < CSS.stick1.top + CSS.stick.height &&
              (CONSTS.ballLeftSpeed[i] = CONSTS.ballLeftSpeed[i] * -1)) ||
              roll("left", i);
          }
  
          if (
            CSS.balls[i].left >=
            CSS.arena.width - CSS.balls[i].width - CSS.stick.width
          ) {
            (CSS.balls[i].top > CSS.stick2.top &&
              CSS.balls[i].top < CSS.stick2.top + CSS.stick.height &&
              (CONSTS.ballLeftSpeed[i] = CONSTS.ballLeftSpeed[i] * -1)) ||
              roll("right", i);
          }
        }
  
        if (
          CONSTS.Stick1DownPressed &&
          CSS.stick1.top >= CSS.arena.height - CSS.stick1.height
        ) {
          //stick1 down
          CSS.stick1.top = CSS.arena.height - CSS.stick1.height;
        } else if (CONSTS.Stick1UpPressed && CSS.stick1.top < 0) {
          //stick1 up
          CSS.stick1.top = 0;
        }
  
        if (
          CONSTS.Stick2DownPressed &&
          CSS.stick2.top >= CSS.arena.height - CSS.stick2.height
        ) {
          //stick2 down
          CSS.stick2.top = CSS.arena.height - CSS.stick2.height;
        } else if (CONSTS.Stick2UpPressed && CSS.stick2.top < 0) {
          //stick2 up
          CSS.stick2.top = 0;
        }
      }, CONSTS.gameSpeed);
    }
  
    function roll(user, ball) {
      if (user == "right") {
        CONSTS.score1++;
        $("#score-board-1").text(CONSTS.score1);
        if (CONSTS.score1 == 5) {
          alert("LEFT SIDE WIN");
          resetScore();
          document.location.reload();
        }
      } else if (user == "left") {
        CONSTS.score2++;
        $("#score-board-2").text(CONSTS.score2);
        if (CONSTS.score2 == 5) {
          alert("RIGHT SIDE WIN");
          resetScore();
          document.location.reload();
        }
      }
  
      $("#pong-ball-" + ball).hide();
      CONSTS.currentBallCount--;
  
      if (CONSTS.currentBallCount == 0) {
        addNewBall();
      }
  
      saveScore();
    }
  
    function loadScore() {
      CONSTS.score1 = localStorage.getItem("score1") || 0;
      CONSTS.score2 = localStorage.getItem("score2") || 0;
    }
    function resetScore() {
      CONSTS.score1 = 0;
      CONSTS.score2 = 0;
    }
    function saveScore() {
      localStorage.setItem("score1", CONSTS.score1);
      localStorage.setItem("score2", CONSTS.score2);
    }
  
    function addNewBall() {
      CSS.balls.push({
        width: 15,
        height: 15,
        position: "absolute",
        top: 300,
        left: 435,
        background: "#C6A62F",
        borderRadius: "50%"
      });
      CONSTS.currentBallCount++;
  
      const last_index = CSS.balls.length - 1;
      $("<div/>", { id: "pong-ball-" + last_index, class: "ball" })
        .css(CSS.balls[last_index])
        .appendTo("#pong-game");
  
      CSS.balls[last_index].top = CSS.arena.height / 2;
      CSS.balls[last_index].left = CSS.arena.width / 2;
  
      var side = -1;
  
      if (Math.random() < 0.5) {
        side = 1;
      }
  
      CONSTS.ballTopSpeed[last_index] = Math.random() * -2 - 3;
      CONSTS.ballLeftSpeed[last_index] = side * (Math.random() * 2 + 3);
    }
  
    start();
  })();
  